<?php
require_once 'autoload.php';

/*
 * -p(parser): тип строк в файле. Допустимые значения описаны константами в интерфейсе IParser;
 *             если тип парсера = hash, то следует указать параметр -x с названием хэш-функции, которая будет приеменятся к каждому слову в файле.
 * -f(file): путь до файла, в котором осуществляется поиск
 * -s,-h,-n: Шаблон для поиска в файле. В зависимости от имени параметра, определяется, что сделать с искомой строкой перед передачей в парсер:
 *  -s(string): оставить как есть. Искать именно такое вхождение. Таким же образом передается Regular Expression.
 *  -h(hash): необходимо искать хэш от переданной строки.
 *            Если указан этот параметр, то также необходимо передать параметр -n,
 *            в котором указать навзание хэш-функции для преобразования шаблона поиска.
 *  -v(validation): путь до файла с описанием разрешений на обрабатываемые файлы(mime_type, max_size);
 *
 * примеры запуска из командной строки:
 * 1)
 * php7.2 run.php -p hash -x sha1 -f example -h valid -n sha1
 *
 * Этой командой парсер будет преобразовыввать каждое слово в строке в хэш(-p hash).
 * В качестве хэш-функции будет использована функция sha1(-x sha1)
 * В качестве файла для поиска указан файл example(-f example)
 * Искомое выражение - 'valid' (-h valid), которое будет преобразовано в хэш(т.к. шаблон задан через -h)
 * В качестве функции для хэширования искомого выражения будет применена фкнкция sha1(-n sha1)
 *
 * ---------------------------------
 * 2)
 * php7.2 run.php -p string -f example -s valid
 *
 * Будет осуществлен обычный поиск по строкам(-p string), в файле example(-f example).
 * Искомое выражение - 'valid'(-s valid).
 *
 *----------------------------------
 *
 * Еще возможные использования парсера:
 * 1) Вместо искомой строки возмжно использование руглярных выражений. Они также задаются флагом -s
 * php7.2 run.php -p string -f example -s ^[val]+
 *
 * 2) Напрмер, у нас есть файл с хэшами и мы хотим найти слово 'valid' в этом файле.
 * php7.2 run.php -p string -f example -h valid -n sha1
 */

$input_params = getopt("p:f:s:h:n:x:v:");

$search_type = null;
$template = null;

if (key_exists("s", $input_params)) {
    $search_type = "string";
    $template = $input_params["s"];
}
if (key_exists("h", $input_params)) {
    $search_type = "hash";
    $template = $input_params["h"];
}

$main = new Main();
$params = array(
    "parser_name" => $input_params["p"],
    "file_path" => $input_params["f"],
    "search_type" => $search_type,
    "template" => $template

);
if (key_exists('n', $input_params)) {
    $params["template_hash"] = $input_params["n"];
}
if (key_exists('x', $input_params)) {
    $params["file_hash"] = $input_params["x"];
}

if (key_exists('v', $input_params)) {
    $params["file_validation_settings"] = $input_params["v"];
}

try {
    $result = $main->run($params);
    echo "RESULT: ".PHP_EOL;
    var_dump($result);
} catch (Exception $exception) {
    echo $exception->getMessage().PHP_EOL;
}

?>
