<?php

namespace app\reg_exp_constructors;

interface IRegExpConstructor
{
    public const CONSTRUCTOR_TYPE_STRING = "string";
    public const CONSTRUCTOR_TYPE_HASH = "hash";

    public function getRegExp(string $template);
}