<?php

namespace app\reg_exp_constructors;

use InvalidArgumentException;

class HashRegExpConstructor implements IRegExpConstructor
{
    private $params;

    public function __construct(array $params)
    {
        if (!$this->isValidParams($params)) {
            throw new InvalidArgumentException("HashRegExpConstructor: incorrect params");
        }
        $this->params=$params;
    }


    public function getRegExp(string $template)
    {
        return '/'.hash($this->params["hash_name"],$template).'/';
    }

    private function isValidParams(array $params) :bool
    {
        if (!key_exists("hash_name", $params)) {
            return false;
        }
        return true;
    }
}