<?php

namespace app\reg_exp_constructors;


class StringRegExpConstructor implements IRegExpConstructor
{

    public function getRegExp(string $template)
    {
        return '/'.$template.'/';
    }
}