<?php

namespace app\reg_exp_constructors;


use InvalidArgumentException;

class RegExpConstructorFactory
{
    private function __construct()
    {
    }

    public static function createRegExpConstructor(string $type, array $params = null) :IRegExpConstructor
    {
        $constructor = null;

        switch ($type) {
            case IRegExpConstructor::CONSTRUCTOR_TYPE_STRING: {
                $constructor = new StringRegExpConstructor();
                break;
            }
            case IRegExpConstructor::CONSTRUCTOR_TYPE_HASH : {
                $constructor = new HashRegExpConstructor($params);
                break;
            }
            default : {
                throw new InvalidArgumentException("Unknown RegExp type: ".$type);
            }
        }

        return $constructor;
    }
}