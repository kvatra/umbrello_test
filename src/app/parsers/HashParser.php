<?php

namespace app\parsers;

class HashParser extends AbstractParser
{
    private $hash_name;

    public function __construct($hash_name)
    {
        $this->hash_name = $hash_name;
    }

    protected function getTransformedString(string $string): string
    {
        $words = explode(' ', $string);
        $result_array = array_map($this->hash_name, $words);
        $result_string = implode(' ', $result_array);

        return $result_string;
    }
}