<?php

namespace app\parsers;

use RangeException;

abstract class AbstractParser implements IParser
{

    function getParseResult(string $file_path, string $reg_exp) :array
    {
        if (!is_readable($file_path)) {
            throw new RangeException("File not found or permission denied");
        }

        $mem_start = memory_get_usage();
        $file = fopen($file_path, 'r');

//        $result = $this->parseFile($file, $reg_exp);

        $result = [];
        $gen = $this->testYield($file,$reg_exp);
        foreach ($gen as $key => $value) {
            $result[$key] = $value;
//            $gen->send('ok');
        }
        $used = memory_get_usage() - $mem_start;
        echo "Memory: ".$used.PHP_EOL;

        fclose($file);
        return $result;
    }

    private function testYield($file, &$reg_exp)
    {
//        $file = fopen($file_path, 'r');

        $file_string = fgets($file);
        for($i = 0;$file_string!==false;$i++) {
            $valid_string = $this->getTransformedString(trim($file_string));
            $position_array = $this->checkString($valid_string, $reg_exp);

            if (count($position_array)>0) {
                $cmd = yield $i => $position_array;
//                if ($cmd != 'ok') {
                    echo $i.PHP_EOL;
//                }
            }

            $file_string = fgets($file);
        }

//        fclose($file);
    }

    protected function parseFile($file, &$reg_exp) :array
    {
        $result = array();

//        $file = fopen($file_path, 'r');
        $file_string = fgets($file);
        for($i = 0;$file_string!==false;$i++) {
            $valid_string = $this->getTransformedString(trim($file_string));
            $position_array = $this->checkString($valid_string, $reg_exp);
            if (count($position_array)>0) {
                $result[$i] = $position_array;
            }
            $file_string = fgets($file);
        }
//        fclose($file);

        return $result;
    }

    protected function checkString(string &$string, &$reg_exp) :array
    {
        $matches = array();
        preg_match_all($reg_exp, $string,$matches,PREG_OFFSET_CAPTURE);

        $result = array();
        foreach ($matches[0] as $match) {
            $result[] = $match[1];
        }

        return $result;
    }

    abstract protected function getTransformedString(string $string) :string;

}