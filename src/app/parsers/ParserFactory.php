<?php


namespace app\parsers;


use InvalidArgumentException;

class ParserFactory
{
    private function __construct()
    {
    }

    public static function createParser(string $parser_name, array $params = null)
    {
        if (empty($parser_name)) {
            throw new InvalidArgumentException("The parser is not defined.");
        }

        $parser = null;
        switch ($parser_name) {
            case IParser::PARSER_TYPE_STRING : {
                $parser = new StringParser();
                break;
            }
            case IParser::PARSER_TYPE_HASH : {
                $hash_name = $params["hash_name"];
                if (empty($hash_name)) {
                    throw new InvalidArgumentException("The hash function name is not defined.");
                }
                $parser = new HashParser($hash_name);
                break;
            }
            default : {
                throw new InvalidArgumentException("Unknown parser name: ".$parser_name);
            }
        }

        return $parser;
    }
}