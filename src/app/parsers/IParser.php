<?php

namespace app\parsers;

interface IParser
{
    public const PARSER_TYPE_STRING = "string";
    public const PARSER_TYPE_HASH = "hash";

    function getParseResult(string $file_path, string $reg_exp) :array;
}