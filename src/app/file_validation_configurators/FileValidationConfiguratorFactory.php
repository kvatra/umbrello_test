<?php

namespace app\file_validation_configurators;


use InvalidArgumentException;

class FileValidationConfiguratorFactory
{
    private function __construct()
    {
    }

    public static function getFileValidationConfigurator(string $file_extension) :IFileValidationConfigurator
    {
        $constructor = null;

        switch ($file_extension) {
            case "yaml" : {
                $constructor = new YamlFileValidationConfigurator();
                break;
            }
            default : {
                throw new InvalidArgumentException("Unsupported file_validation_extension: ".$file_extension);
            }
        }

        return $constructor;
    }
}