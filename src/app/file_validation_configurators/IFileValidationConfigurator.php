<?php

namespace app\file_validation_configurators;


interface IFileValidationConfigurator
{
    function getFileValidationConfiguration(string $file_path) :array;
}