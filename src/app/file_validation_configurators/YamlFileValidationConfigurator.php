<?php

namespace app\file_validation_configurators;

class YamlFileValidationConfigurator implements IFileValidationConfigurator
{

    function getFileValidationConfiguration(string $file_path): array
    {
        return yaml_parse_file($file_path);
    }
}