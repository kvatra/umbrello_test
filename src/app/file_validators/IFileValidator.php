<?php

namespace app\file_validators;

interface IFileValidator
{
    public const FILE_VALIDATOR_NAME = "default";

    function isValidFile(string &$file_path) :bool;
}