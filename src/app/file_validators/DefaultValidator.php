<?php

namespace app\file_validators;

use RangeException;
use UnexpectedValueException;

class DefaultValidator implements IFileValidator
{
    private $params;

    public function __construct(array $params)
    {
        $this->checkParams($params);
        $this->params = $params;
    }

    private function checkParams(array $params)
    {
        if (!key_exists("mime_type", $params)) {
            throw new RangeException("The mime-type parameter not found");
        }
        if (!key_exists("max_size", $params)) {
            throw new RangeException("The max_size parameter not found");
        }
    }

    function isValidFile(string &$file_path): bool
    {
        /*
         * mime-type
         * max_size
         */

        if (!is_readable($file_path)) {
            throw new UnexpectedValueException("File not found or permission denied.");
        }

        $max_size = $this->params["max_size"];
        $file_size = filesize($file_path);
        if ($file_size>$max_size) {
            throw new RangeException("Exceeded the maximum file size.");
        }

        $mime_type = $this->params["mime_type"];
        $file_mime_type = mime_content_type($file_path);
        if ($mime_type !== $file_mime_type) {
            throw new RangeException("Incorrect file mime type: ".$file_mime_type);
        }

        return true;
    }
}