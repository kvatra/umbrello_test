<?php

namespace app\file_validators;

use InvalidArgumentException;

class FileValidatorFactory
{

    private function __construct()
    {
    }

    public static function getFileValidator(string $name, array $params) :IFileValidator
    {
        $validator = null;

        switch ($name) {
            case IFileValidator::FILE_VALIDATOR_NAME : {
                $validator = new DefaultValidator($params);
                break;
            }
            default : {
                throw new InvalidArgumentException("Unsupported file_validator name: ".$name);
            }
        }

        return $validator;
    }

}