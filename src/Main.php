<?php

use app\file_validation_configurators\FileValidationConfiguratorFactory;
use app\file_validators\FileValidatorFactory;
use app\file_validators\IFileValidator;
use app\parsers\ParserFactory;
use app\reg_exp_constructors\RegExpConstructorFactory;

class Main
{
    public function run(array $params) :array
    {
        $parser_params = $this->getParserConfiguration($params);
        $reg_exp_constructor_params = $this->getRegExpConstructorConfiguration($params);

        $this->checkFile($params);

        $result = array();
        try {
            $parser_name = $params["parser_name"];
            $template = $params["template"];
            $search_type = $params["search_type"];
            $file_path = $params["file_path"];

            $reg_exp = $this->getRegExp($search_type, $template, $reg_exp_constructor_params);
            $parser = ParserFactory::createParser($parser_name, $parser_params);
            $result = $parser->getParseResult($file_path, $reg_exp);
        } catch (Exception $ex) {
            echo "Exception: ".$ex->getMessage().PHP_EOL;
        }

        return $result;
    }

    private function checkFile($params) :bool
    {
        if (key_exists("file_validation_settings", $params)) {
            $validation_settings = $this->getFileValidationSettings($params["file_validation_settings"]);
            $validator = FileValidatorFactory::getFileValidator(IFileValidator::FILE_VALIDATOR_NAME, $validation_settings);

            $file_path = $params["file_path"];
            $validator->isValidFile($file_path);
        }

        return true;
    }

    private function getFileValidationSettings(string $validation_settings_path)
    {
        $file_extension = pathinfo($validation_settings_path, PATHINFO_EXTENSION);
        $configurator = FileValidationConfiguratorFactory::getFileValidationConfigurator($file_extension);
        $settings = $configurator->getFileValidationConfiguration($validation_settings_path);

        return $settings;
    }

    private function getRegExpConstructorConfiguration(array $params)
    {
        $reg_exp_constructor_params = array();
        if (key_exists("template_hash", $params)) {
            $reg_exp_constructor_params = array(
                "hash_name" => $params["template_hash"]
            );
        }

        return $reg_exp_constructor_params;
    }

    private function getParserConfiguration(array $params)
    {
        $parser_params = array();
        if (key_exists("file_hash", $params)) {
            $parser_params = array(
                "hash_name" => $params["file_hash"]
            );
        }

        return $parser_params;
    }

    private function getRegExp(string $type, string $template, array $constructor_params=array()) :string
    {
        $constructor = RegExpConstructorFactory::createRegExpConstructor($type, $constructor_params);
        $reg_exp = $constructor->getRegExp($template);
        return $reg_exp;
    }
}